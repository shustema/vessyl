# Purpose
The purpose of this file is to loosely define how wallets will be converted to the blockchain in a secure manner. 
This file will discus the layers of encryption used to ensure at no step in the process can the private key be obtained by a bad actor

# General Information 
## Bitcoin Wallets
Bitcoin wallets, upon creation generate a public and private key. The public key is the "receiving address" that is used to send money to this wallet.
The private key is used for sending, and is the mechanism which defines ownership. For Vessyl to be secure, at no point in the distribution process 
can the private key be accessable to anyone. The only time that private key should be directly accessed, should be when the umint process is initiated. 

## Private Key Storage
Private keys much be stored at some point in this process so that a wallet does not become a dead wallet. To do this, we propose the following protocol 

# Processes 
## Mint
The private key will be generated, and pasphrases will also be generated. After this point the private key will be destroyed. These pasphrases will then be 
uploaded individually to a destributed network (storj maybe) such that no single computer maintains the private key. The encryption key for that storj commit
will also be uploaded to seperate systems (if storj is to be used, we will need to modify the protocol so that instead of an encryption key being directly used,
it will instead unlock the data if the blockchain supports the ownership claim the wallet is making.)

## UMint
To unmint a Vessyl, the ownership of the wallet will be checked on the blockchain. If ownership is verified by the network, then the distributed pasphrases and encryption
keys will be disseminated to the wallet owner. At this point, the local owner will reassemble the private key, and transfer all coin to the specified bitcoin account, 
after which point the Vessyl wallet will be burned. 

## Transfer
as the pasphrases and encryption keys are locked behind the P2P netowrk, transfering will not require any calls to the bitcoin network. These transfers can then work on the
Vessyl blockchain in the same way as an NFT would work. 

